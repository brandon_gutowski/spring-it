﻿//Below is an example of what you should get from the tiled level editor when exported
//The first set should be 8 layers similiar to what is shown below
//{ "height":15,
//"layers":[
//   {
//	"data":[0,0,...]
//"height":15,
//"name":"Level Layer",
//"opacity":1,
//"type":"tilelayer",
//"visible":true,
//"width":20,
//"x":0,
//"y":0
//}, 
// 
//Below this point shows what each fo the tile sets will look like, there should be 8 of them in total as well.  
//The difference with them is that their identifying attribute "firstgrid" will nto increase by 1 for each tile set becasue many of the sets have more than 1 tile.
//"tileheight":256,
//"tilesets":[
// {
//	"firstgid":1,
//	"image":"Tilesets\/Conveyor\/Conveyor.png",
//	"imageheight":256,
//	"imagewidth":512,
//	"margin":0,
//	"name":"Conveyor",
//	"properties":
//	{
//		
//	},
//	"spacing":0,
//	"tileheight":256,
//	"tilewidth":256
// }, 
//
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class UnpackLevel : MonoBehaviour {

	public int mNumberOfTileSets = 10;
	public int mLevelScaling = 2;
	public TextAsset levelText;

	private Dictionary<string, object> mJSONData;

	//Stuff for loading
	private long mLevelWidth;
	private long mLevelHeight;
	private List<string> mTileNames;

	private List<int> mLevelLayerDataLocations = new List<int>();
	private List<int> mStructuringDataLocations = new List<int>();
	private List<int> mPlaceableDataLocations = new List<int>();
	private List<int> mBackgroundDataLocations = new List<int>();
	private List<int> mAvailiblePrefabs = new List<int>();
	private List<int> mRobotDataLocations = new List<int>();
	private List<int> mBudgetDataLocations = new List<int>();
	private List<int> mInterractableDataLocations = new List<int>();

	//Stuff for building
	private List<GameObject> mLoadedResources = new List<GameObject>();
	//private List<GameObject> mSceneItems = new List<GameObject>();


	void Start(){

		Unpack(levelText);
	
	}

	public void Unpack(TextAsset levelToLoad) {

		mJSONData = MiniJSON.Json.Deserialize(levelToLoad.text) as Dictionary<string, object>;
		UnpackTileSets();
		UnpackLocations();
		BuildLevelLayer();
		BuildStructuringLevel();
		//BuildPlaceableLayer();
		BuildBackgroundLayer();


	}

	void UnpackTileSets(){
	
		mTileNames = new List<string>();

		for(int i = 0; i < mNumberOfTileSets; i++){
			mTileNames.Add(((mJSONData["tilesets"] as List<object>)[i] as Dictionary<string, object>)["name"].ToString());
		}
	}

	// Each one of the for loops below are manually added for each new layer added to the editor
	void UnpackLocations(){

		mLevelWidth = (long)((mJSONData["layers"] as List<object>)[1] as Dictionary<string, object>)["width"];
		mLevelHeight = (long)((mJSONData["layers"] as List<object>)[1] as Dictionary<string, object>)["height"];

	
		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[0] as Dictionary<string, object>)["data"] as List<object>)[i];
			mLevelLayerDataLocations.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[1] as Dictionary<string, object>)["data"]as List<object>)[i];
			mStructuringDataLocations.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[2] as Dictionary<string, object>)["data"]as List<object>)[i];
			mPlaceableDataLocations.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[3] as Dictionary<string, object>)["data"]as List<object>)[i];
			mBackgroundDataLocations.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[4] as Dictionary<string, object>)["data"]as List<object>)[i];
			mAvailiblePrefabs.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[5] as Dictionary<string, object>)["data"]as List<object>)[i];
			mRobotDataLocations.Add((int)dataMiddleMan);
		}

		for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
			long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[6] as Dictionary<string, object>)["data"]as List<object>)[i];
			mBudgetDataLocations.Add((int)dataMiddleMan);
		}

		for(int j = 7; j < (mJSONData["layers"] as List<object>).Count;j++){
			for( int i = 0; i < mLevelHeight*mLevelWidth; i++){
				long dataMiddleMan = (long)(((mJSONData["layers"] as List<object>)[j] as Dictionary<string, object>)["data"]as List<object>)[i];
				mInterractableDataLocations.Add((int)dataMiddleMan);
			}
		}
	}

	void BuildLevelLayer(){

		IdentifyTiles();
		InstantiateTheLevelObjects();
		BuildSceneItems();


	}

	void IdentifyTiles(){
		for(int i = 0; i < mNumberOfTileSets;i++){

			if(Resources.Load ("Prefabs/" + mTileNames[i], typeof(GameObject)) != null){
				mLoadedResources.Add (Resources.Load("Prefabs/" + mTileNames[i], typeof(GameObject)) as GameObject);
			}
		}
	}

	void InstantiateTheLevelObjects(){

		for(int i = 0; i < mLevelLayerDataLocations.Count; i++){
			GameObject tempObject;																																																																																																								
			int switchVariable = 0;

			//This is a manual way for me to match each tile found to which type of item is being created for the Level layer Only
			if(mLevelLayerDataLocations[i] == 0) switchVariable = 0;
			if((mLevelLayerDataLocations[i]== 1) || (mLevelLayerDataLocations[i] == 2)) switchVariable = 1;//Conveyor
			if((mLevelLayerDataLocations[i]== 3) || (mLevelLayerDataLocations[i] == 4)) switchVariable = 2; //Spring
			if((mLevelLayerDataLocations[i] > 4) && (mLevelLayerDataLocations[i] < 9)) switchVariable = 3;//Fan
			if(mLevelLayerDataLocations[i] == 9) switchVariable = 4;//Spawner
			if(mLevelLayerDataLocations[i] == 10) switchVariable = 5;//Collector
			if(mLevelLayerDataLocations[i] == 11) switchVariable = 6; //Robot

			switch(switchVariable){

			default:
				break;

			case 0:
				break;

			case 1: //Conveyor

				tempObject = Instantiate(mLoadedResources[0],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[0].name;

				if(mLevelLayerDataLocations[i] == 1){
					tempObject.transform.Rotate(0,180, 0);
				}
				break;

			case 2: //Spring
				tempObject = Instantiate(mLoadedResources[1],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[1].name;
				
				if(mLevelLayerDataLocations[i] == 3){
					tempObject.transform.Rotate(0,180, 0);
				}
				break;

			case 3: //Fan
				tempObject = Instantiate(mLoadedResources[2],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[2].name;
				
				if(mLevelLayerDataLocations[i] == 5){
					tempObject.transform.Rotate(0, 0, 90);
				}

				if(mLevelLayerDataLocations[i] == 7){
					tempObject.transform.Rotate(0, 180, 0);
				}

				if(mLevelLayerDataLocations[i] == 8){
					tempObject.transform.Rotate(0, 0, -90);
				}
				break;

			case 4: //Spawner
				tempObject = Instantiate(mLoadedResources[3],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[3].name;

				break;

			case 5: //Collector
				tempObject = Instantiate(mLoadedResources[4],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[4].name;
			
				break;

			case 6: //Robot
				tempObject = Instantiate(mLoadedResources[5],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[5].name;
				
				break;

			}
		}
	}

	Vector3 getInstantiatePosition(int incomingData){

		Vector3 tempVector = new Vector3(0,0,0);

		tempVector.x = (incomingData % mLevelWidth) * mLevelScaling;
		tempVector.y = mLevelScaling * (mLevelHeight - (incomingData/mLevelWidth));

		return tempVector;
	}

	void BuildSceneItems(){

		GameObject mainCamera = Resources.Load ("Prefabs/Main Camera", typeof(GameObject)) as GameObject;
		GameObject endGameCamera = Resources.Load("Prefabs/EndGameCamera", typeof(GameObject))as GameObject;
		GameObject background = Resources.Load ("Prefabs/background", typeof(GameObject)) as GameObject;
		GameObject light = Resources.Load ("Prefabs/Directional light", typeof(GameObject)) as GameObject;

		//These are all of the variables that need to be set on the Main Camera's scripts
		GameObject tempObject = Instantiate(mainCamera, new Vector3(mLevelScaling*mLevelWidth/2,mLevelScaling*mLevelHeight,-30), Quaternion.identity) as GameObject;
		tempObject.GetComponent<PlayerObjectButtons>().mEnableEachObject = EnablePlaceables();
		tempObject.GetComponent<Budget>().mTotalBudget = SetBudget();
		tempObject.GetComponent<MainCameraGUI>().mRobotsNeededToWin = SetRobotsNeededToWin();
		tempObject.GetComponent<MainCameraGUI>().mMaxRobotsReleased = SetRobotsReleased();
		tempObject.GetComponent<CameraMovement>().mLevelSize.x = mLevelWidth * 2;
		tempObject.GetComponent<CameraMovement>().mLevelSize.y = mLevelHeight * 2;



		Instantiate(light, light.transform.position, light.transform.rotation);
		Instantiate(background, background.transform.position, Quaternion.identity);
	}

	bool[] EnablePlaceables(){

		bool[] tempbool = {false, false, false};

		for(int i = 0; i < mLevelHeight * mLevelWidth;i++){

			if((mAvailiblePrefabs[i]== 1) || (mAvailiblePrefabs[i] == 2)) tempbool[2] = true;//Conveyor
			if((mAvailiblePrefabs[i]== 3) || (mAvailiblePrefabs[i] == 4)) tempbool[0] = true; //Spring
			if((mAvailiblePrefabs[i] > 4) && (mAvailiblePrefabs[i] < 9)) tempbool[1] = true;//Fan

		}

		return tempbool;
	}

	int SetBudget(){

		int budget = 0;

		for(int i = 0; i < mLevelHeight * mLevelWidth;i++){

			if(mBudgetDataLocations[i] == 13) budget += 100;
			if(mBudgetDataLocations[i] == 14) budget += 500;
			if(mBudgetDataLocations[i] == 15) budget += 1000;
			if(mBudgetDataLocations[i] == 16) budget += 10000;
		}

		return budget;
	}

	int SetRobotsNeededToWin(){

		int robotsToWin = 0;

		for(int i = 0; i < (mLevelHeight * mLevelWidth)/2;i++){
			
			if(mRobotDataLocations[i] == 11) robotsToWin++;//11 is the value for the robot

		}

		return robotsToWin;

	}

	int SetRobotsReleased(){

		int robotsReleased = 0;

		for(int i = (int)(mLevelHeight*mLevelWidth)/2; i < (mLevelHeight * mLevelWidth);i++){
			
			if(mRobotDataLocations[i] == 11) robotsReleased++;//11 is the value for the robot
			
		}

		return robotsReleased;
	}

	void BuildStructuringLevel(){

		for(int i = 0; i < mStructuringDataLocations.Count; i++){
			GameObject tempObject;																																																																																																								
			int switchVariable = 0;
			
			//This is a manual way for me to match each tile found to which type of item is being created for the Level layer Only
			if((mStructuringDataLocations[i] == 1) ||(mStructuringDataLocations[i] == 2)) switchVariable = 0;
			if(mStructuringDataLocations[i]== 17) switchVariable = 1;//Floors
			if(mStructuringDataLocations[i]==18) switchVariable = 2;//Walls
						
			switch(switchVariable){
				
			default:
				break;
				
			case 0:
				break;
				
			case 1: //Floors
				
				tempObject = Instantiate(mLoadedResources[7],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[7].name;
				
				break;
				
			case 2: //Walls
				tempObject = Instantiate(mLoadedResources[8],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[8].name;
				tempObject.transform.Rotate(-90, 0, 90);

				break;
			
			}
		}

	}

	void BuildBackgroundLayer(){
		
		for(int i = 0; i < mStructuringDataLocations.Count; i++){
			GameObject tempObject;																																																																																																								
			int switchVariable = 0;
			
			//This is a manual way for me to match each tile found to which type of item is being created for the Level layer Only
			if((mStructuringDataLocations[i] == 1) ||(mStructuringDataLocations[i] == 2)) switchVariable = 0;
			if(mStructuringDataLocations[i]== 12) switchVariable = 1;//Furnace
			
			switch(switchVariable){
				
			default:
				break;
				
			case 0:
				break;
				
			case 1: //Furnace
				
				tempObject = Instantiate(mLoadedResources[6],getInstantiatePosition(i), Quaternion.identity) as GameObject;
				tempObject.name = mLoadedResources[6].name;
				
				break;
					
				
			}
		}
		
	}
}
