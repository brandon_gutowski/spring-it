﻿using UnityEngine;
using System.Collections;

public class TextureSlider : MonoBehaviour {
	public Transform target;
	public float offsetX;
	public float offsetY;
	
	private Material mater;
	private Vector2 offset;

	// Use this for initialization
	void Start () {
		mater = target.renderer.material;
		this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		offset.x += offsetX * Time.deltaTime;
		offset.y += offsetY * Time.deltaTime;
		
		mater.SetTextureOffset("_MainTex", offset);
	}
}
