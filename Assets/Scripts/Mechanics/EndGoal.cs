﻿using UnityEngine;
using System.Collections;

public class EndGoal : MonoBehaviour {
		
	void OnTriggerEnter(Collider collider){
		
		if(collider.gameObject.tag == "Robot"){
			Destroy(collider.gameObject);
			Camera.main.GetComponent<MainCameraGUI>().ScoreRobot();
			
		}
	}
}
