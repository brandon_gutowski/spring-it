﻿using UnityEngine;
using System.Collections;

public class FallingFloor : MonoBehaviour {
	
	public float mTimeUntilDestroyed;
	public float mTimeUntilFall;
	
	private float mTimeSinceTrigger;
	private float mTimeWhenTriggered;
	private bool mFloorTriggered = false;
	
	void OnTriggerEnter(Collider collider){
				
		if(collider.gameObject.tag == "Robot"){
			
			if(mFloorTriggered == false){
				mTimeWhenTriggered = Time.time;
			}
			mFloorTriggered = true;
		}
	}
	
	void Update() {
		
		
		if(mFloorTriggered == true){
			
			mTimeSinceTrigger = Time.time - mTimeWhenTriggered;
		
			if(mTimeSinceTrigger >= mTimeUntilFall){
				this.GetComponentInChildren<Rigidbody>().useGravity = true;
				this.GetComponentInChildren<Rigidbody>().isKinematic = false;
			}
			
			if((Time.time - mTimeWhenTriggered) >= (mTimeUntilFall + mTimeUntilDestroyed)){
				Destroy (this.gameObject);
				
			}
		}
	}
	
}
