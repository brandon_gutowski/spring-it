﻿using UnityEngine;
using System.Collections;

public class Fan : MonoBehaviour {
	
	public float mFanSpeed;
	private Vector3 mFanDirection;	
	
	//I don't know if we want this to add a static amount of velocity each frame or add more for faster objects and less for slower.
	//public float mAverageRobotSpeed;
	//private float mRobotsVelocity;
	
	void Start () {

		mFanDirection = (transform.position - transform.parent.transform.position).normalized;
		
	}
	void OnTriggerStay(Collider collider){
		
		if(collider.gameObject.tag == "Robot"){
			collider.attachedRigidbody.AddForce (mFanDirection * mFanSpeed);	
		}
	}

}
