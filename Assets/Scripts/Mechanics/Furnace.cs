﻿using UnityEngine;
using System.Collections;

public class Furnace : MonoBehaviour {
	
	void Start() {
		this.enabled = false;	
	}
	
	void OnTriggerEnter(Collider collider){
		
		if(collider.gameObject.tag == "Robot"){
			Destroy(collider.gameObject);
		}
	}
}
