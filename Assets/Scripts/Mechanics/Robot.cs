﻿using UnityEngine;
using System.Collections;

public class Robot : MonoBehaviour {
	
	private int mLockedPosition = 0;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		 transform.rotation = Quaternion.Euler(mLockedPosition,mLockedPosition, mLockedPosition);
	}
}
