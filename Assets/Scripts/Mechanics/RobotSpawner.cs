﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RobotSpawner : MonoBehaviour {
	
	public GameObject mRobot;
	public float mSpawntime;
	public Transform mSpawnPoint;
	public float mSpawnStartDelay;
	
	private float mNextSpawn;
	private List<GameObject> mRobotList = new List<GameObject>();
	private bool mCanSpawnRobot = false;
	private float mTimeSinceCreation = 0.0f;
		
	void Start(){

		this.enabled = false;
		mCanSpawnRobot = true;
		mNextSpawn = mSpawntime;

				
	}
		
	void Update () {
		mTimeSinceCreation += Time.deltaTime;



		if(mTimeSinceCreation >= mSpawnStartDelay){

			if(mTimeSinceCreation > (mNextSpawn - 2.0f)){
				//animation.Play();
			}
			if(mTimeSinceCreation > (mNextSpawn)){
				mCanSpawnRobot = Camera.main.GetComponent<MainCameraGUI>().SpawnRobot();
				if(mCanSpawnRobot == true){
					mRobotList.Add(Instantiate(mRobot, mSpawnPoint.position, mSpawnPoint.rotation) as GameObject);
					mTimeSinceCreation =Time.time;
					mNextSpawn = mTimeSinceCreation + mSpawntime;

				}
			}
		}	
	}	
}
