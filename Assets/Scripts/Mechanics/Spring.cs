﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour {
	
	public float mLaunchHeight;
	public float mLaunchDistance;
	public Animation mAttachedAnimation;

	private float mXLaunchSpeed;
	private float mYLaunchSpeed;
	private Vector3 mRobotTravelDirection = new Vector3(0,0,0);
	
	void Start(){
		CalculateLaunchSpeeds();
		this.enabled = false;

	
	}
		
	void OnTriggerStay(Collider collider){
		
		if(collider.gameObject.tag == "Robot"){
			mRobotTravelDirection = collider.attachedRigidbody.velocity;
			CalculateLaunchSpeeds();
			collider.attachedRigidbody.velocity = new Vector3 (mXLaunchSpeed, mYLaunchSpeed, 0);
			mAttachedAnimation.Play();
		}
	}
	
	void CalculateLaunchSpeeds(){
		
		float gravity = Physics.gravity.magnitude;
		float totalFlightTime;

		mRobotTravelDirection.y = 0;
		mRobotTravelDirection.z = 0;
		mRobotTravelDirection = mRobotTravelDirection.normalized;

		mYLaunchSpeed = Mathf.Sqrt (2f *gravity *mLaunchHeight);
		totalFlightTime = 2f * mYLaunchSpeed/gravity;
		mXLaunchSpeed = mRobotTravelDirection.x * mLaunchDistance/totalFlightTime;
		
	}
}
