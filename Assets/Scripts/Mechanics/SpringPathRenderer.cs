﻿/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpringPathRenderer : MonoBehaviour {
	
	public int mNumberOfLineDivisions;
	//public Material mMaterial;
	//public int mLineWidth;
	//public Color mLineColor;
	
	private Vector3[] mLinePositions;
	private Vector3[] mLineSubPositions;
	private float mSpringHeight;
	private float mSpringDistance;
	
	void Start(){
		
		mSpringHeight = gameObject.GetComponent<Spring>().mLaunchHeight;
		mSpringDistance = gameObject.GetComponent<Spring>().mLaunchDistance;
		
		mLineSubPositions = new Vector3[mNumberOfLineDivisions];
		
		LineRenderer mPathLineRenderer = gameObject.AddComponent<LineRenderer>();
		mPathLineRenderer.material = mMaterial;
		mPathLineRenderer.SetColors(mLineColor, mLineColor);
		mPathLineRenderer.SetWidth(mLineWidth,mLineWidth);
		mPathLineRenderer.SetVertexCount(mNumberOfLineDivisions);
		
		//CreateLineRenderer();
		
	}
		
	void CreateLineRenderer(){
		
	}
	
	void OnDrawGizmosSelected(){
		mLinePositions = new Vector3[3];
		mLinePositions[0] = transform.position;
		mLinePositions[1] = new Vector3((transform.position.x + mSpringDistance/2.0f), (transform.position.y + mSpringHeight), transform.position.z);
		mLinePositions[2] = new Vector3(transform.position.x + mSpringDistance, transform.position.y, transform.position.z);
		
		for(int sub = 0; sub <= mNumberOfLineDivisions; sub++){
			float firstPoint = sub/mNumberOfLineDivisions;
			float secondPoint = (1 + sub)/mNumberOfLineDivisions;
			
			Vector3 gizmoPoint1 = CurveSegment(firstPoint);
			Vector3 gizmoPoint2 = CurveSegment(secondPoint);
			
			Gizmos.DrawLine(gizmoPoint1, gizmoPoint2);
		}	
	}
	
	Vector3 CurveSegment(float time){
		
		Vector3 tempVector = new Vector3(0,0,0);
		
		tempVector.x = (mLinePositions[2].x - mLinePositions[0].x)* time/mNumberOfLineDivisions;
		tempVector.z = transform.position.z;
		
		if(transform.position.x <= mLinePositions[1].x){
			tempVector.y = (mLinePositions[1].y - mLinePositions[0].y) *time/mNumberOfLineDivisions;
		}
		else{
			tempVector.y = (mLinePositions[2].y - mLinePositions[1].y) *time/mNumberOfLineDivisions;	
		}
		
		return tempVector;
	}
}*/
