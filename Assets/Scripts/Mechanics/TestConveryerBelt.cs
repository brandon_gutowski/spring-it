﻿using UnityEngine;
using System.Collections;

public class TestConveryerBelt : MonoBehaviour {

	public int mConveyerSpeed;
	
	private Vector3 mCurrentVelocity;

	private Vector3 mMoveDirection;
	
	void Start(){
		this.enabled = false;

		UpdateMovingDirection();
	}
	
	void OnTriggerStay(Collider collider){


		if(collider.gameObject.tag == "Robot"){

			UpdateMovingDirection();
			mCurrentVelocity = collider.attachedRigidbody.velocity;
			collider.attachedRigidbody.velocity = new Vector3 (mMoveDirection.x * mConveyerSpeed, mCurrentVelocity.y, 0);

		}
	}

	void UpdateMovingDirection(){

		mMoveDirection = new Vector3(0,0,0);

		mMoveDirection.x = transform.position.x - transform.parent.transform.position.x;

		mMoveDirection = mMoveDirection.normalized;

	
	}
}
