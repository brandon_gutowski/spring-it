﻿/*using UnityEngine;
using System.Collections;

public class TrapConveyor : MonoBehaviour {

	//public int mFallDelay;
	private Collider[] mEdgeColliders;
	private GameObject mConveyorClosed;
	private GameObject mConveyorOpen;
	public Texture2D mButtonTexture;
	
	private Vector2 mButtonSize;
	private bool mTrapOpen;
	
	
	void Start () {
		//This makes sure the buttons are the same size and only controlled by the other script
		mButtonSize.x = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MainCameraGUI>().mButtonX;
		mButtonSize.y = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MainCameraGUI>().mButtonY;
		
		int totalColliders = transform.Find("TrapColliders").childCount;
		mEdgeColliders = new Collider[totalColliders];
		
		for(int i = 0; i<mEdgeColliders.Length;i++){
			mEdgeColliders[i] = transform.Find("TrapColliders").GetChild(i).collider;
			mEdgeColliders[i].enabled = false;	
		}
		
		//This Enables the Conveyor Model and disables the Trap Conveyor Model
		Transform tempConveyor = transform.FindChild ("Conveyor");
		tempConveyor.gameObject.SetActive(true);
		Transform tempTrap = transform.FindChild ("TrapConveyor");
		tempTrap.gameObject.SetActive(false);

		mTrapOpen = false;
		
	}
			
	void OnGUI(){
		
		if(GUI.Button(new Rect((50), (50),mButtonSize.x ,mButtonSize.y), mButtonTexture)){
			OpenClose(mTrapOpen);
		}	
	}
	
	void OpenClose(bool newTrapStatus){
		this.GetComponentInChildren<TestConveryerBelt>().enabled = mTrapOpen;
		this.GetComponentInChildren<TrapConveyorStop>().enabled = mTrapOpen;
		for(int i = 0; i<mEdgeColliders.Length;i++){
			mEdgeColliders[i].enabled = mTrapOpen;	
		}
		
		//Change what model is shown
		Transform tempConveyor = transform.FindChild ("Conveyor");
		tempConveyor.gameObject.SetActive(mTrapOpen);
		Transform tempTrap = transform.FindChild ("TrapConveyor");
		tempTrap.gameObject.SetActive(!mTrapOpen);

		mTrapOpen = !newTrapStatus;
	}
	
	void Reset(){
		this.GetComponentInChildren<TestConveryerBelt>().enabled = true;
		this.GetComponentInChildren<TrapConveyorStop>().enabled = true;
		for(int i = 0; i<mEdgeColliders.Length;i++){
			mEdgeColliders[i].enabled = false;	
		}
		
		Transform tempConveyor = transform.FindChild ("Conveyor");
		tempConveyor.gameObject.SetActive(true);
		Transform tempTrap = transform.FindChild ("TrapConveyor");
		tempTrap.gameObject.SetActive(false);
			
	}

}*/
