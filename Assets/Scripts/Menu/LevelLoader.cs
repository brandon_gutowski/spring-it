﻿using UnityEngine;
using System.Collections;

public class LevelLoader {

	public const string GameScene = "GameScene";	

	public static LevelLoader instance
	{
		get
		{
			if(LevelLoader.internalInstance == null)
			{
				LevelLoader.internalInstance = new LevelLoader();				
			}

			return LevelLoader.internalInstance;
		}
	}

	private static LevelLoader internalInstance;

	private LevelLoader() { ; }

	public void LoadLevel(TextAsset level) {

		Application.LoadLevel(LevelLoader.GameScene);
		GameObject levelUnpacker = new GameObject();

		UnpackLevel unpacker = levelUnpacker.AddComponent<UnpackLevel>();
		unpacker.Unpack(level);
	}
}
