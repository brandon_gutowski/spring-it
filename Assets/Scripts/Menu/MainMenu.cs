﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public string levelPrefix;
	public GameObject levelItemPrefab;
	public UIGrid levelGrid;

	// Use this for initialization
	void Start () {
		
		Debug.Log("Starting load");
		Transform gridTransform = levelGrid.transform;

		int currentLevelSuffix = 1;

		Debug.Log("Trying to load a text asset with name " + this.levelPrefix + currentLevelSuffix.ToString());

		TextAsset currentLevel = Resources.Load<TextAsset>(this.levelPrefix + currentLevelSuffix.ToString());
		while(currentLevel != null) {

			GameObject newLevelItem = GameObject.Instantiate(this.levelItemPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			newLevelItem.transform.parent = gridTransform;
			newLevelItem.transform.localScale = Vector3.one;

			MenuLevelItem menuScript = newLevelItem.GetComponent<MenuLevelItem>();
			menuScript.levelToLoad = currentLevel;

			++currentLevelSuffix;
			currentLevel = Resources.Load<TextAsset>(this.levelPrefix + currentLevelSuffix.ToString());
		}


		Debug.Log("All done yo");
		this.levelGrid.Reposition();
	}
}
