﻿using UnityEngine;
using System.Collections;

public class MenuLevelItem : MonoBehaviour {

	public GameObject playButton;
	public TextAsset levelToLoad { get; set; }

	// Use this for initialization
	void Start () {
		
		UIEventListener.Get(this.playButton).onClick += this.LoadLevel;
	}
	
	private void LoadLevel(GameObject buttonPressed){
		LevelLoader.instance.LoadLevel(this.levelToLoad);
	}
}
