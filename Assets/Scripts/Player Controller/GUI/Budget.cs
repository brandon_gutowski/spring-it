﻿using UnityEngine;
using System.Collections;

public class Budget : MonoBehaviour {

	public int mTotalBudget;
	public int[] mBudgetAmounts;
	public GameObject mBudgetLabel;

	private int mRemainingBudget;

	void Start(){

		mRemainingBudget = mTotalBudget;

	}

	void Update(){

		mBudgetLabel.GetComponent<UILabel>().text = "$" + mRemainingBudget.ToString ();

	}

	public void PlaceObject(int identifier){

		mRemainingBudget -= mBudgetAmounts[identifier];

	}

	public void DeleteObject(int identifier){

		mRemainingBudget += mBudgetAmounts[identifier];

	}

	public bool AbleToPlace(int identifier){
	
		if(mRemainingBudget < mBudgetAmounts[identifier]){
			return true;
		}

		return false;

	}
}
