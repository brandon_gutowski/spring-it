﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
	
	public float mMouseMoveZone = 10;
	public float mCameraScrollSpeed = 15;

	public Vector2 mCameraOffset= new Vector2(6, 8);	
	public Vector2 mLevelSize;
	
	void Update () {
		
		ShouldCameraMove();
			
	}
	
	void ShouldCameraMove(){
		if((Input.mousePosition.x >= Screen.width - mMouseMoveZone) && CheckBoundaries("right")){
				transform.position += Vector3.right * Time.deltaTime * mCameraScrollSpeed;
		}
		if((Input.mousePosition.x <= 0 + mMouseMoveZone) && CheckBoundaries("left")){
				transform.position += Vector3.left * Time.deltaTime * mCameraScrollSpeed;
		}
		if((Input.mousePosition.y <= 0 + mMouseMoveZone) && CheckBoundaries("down")){
				transform.position += Vector3.down * Time.deltaTime * mCameraScrollSpeed;
		}
		if((Input.mousePosition.y >= Screen.height + mMouseMoveZone) && CheckBoundaries("up")){
				transform.position += Vector3.up * Time.deltaTime * mCameraScrollSpeed;
		}
		
	}
		
	bool CheckBoundaries(string directionIdentifier){
		Vector3 cameraLocation = camera.transform.position;
		switch(directionIdentifier){
			
		case "down" : if( cameraLocation.y <= mCameraOffset.y){
				return false;
				}
				break;
		
		case "left" : if( cameraLocation.x <= mCameraOffset.x){
				return false;			
				}
				break;
		
		case "right":  if( cameraLocation.x >= (mLevelSize.x)){
				return false;			
				}
				break;
		
		case "up": if(cameraLocation.y >= (mLevelSize.y)){
				return false;
				}
				break;
		}
		return true;
	}
}
