﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class LevelRestart : MonoBehaviour {
	
	public string mNextScene;
	public Vector2 mButtonSize;
	public int mPixelsFromCenter;
	public GameObject mMainCamera;
	//public GUIStyle mGUIStyle;
	
	private Rect mResetButton;
	private Rect mNextLevelButton;
	private Rect mLabelArea;
	
	void Start(){
		
		mResetButton = new Rect((Screen.width/2 - mButtonSize.x/2 - mPixelsFromCenter), (Screen.height/2), mButtonSize.x, mButtonSize.y);
		mNextLevelButton = new Rect((Screen.width/2 + mButtonSize.x/2 + mPixelsFromCenter), (Screen.height/2), mButtonSize.x, mButtonSize.y);
		mLabelArea = new Rect(mResetButton.x, mResetButton.y - 9/8 * mButtonSize.y, (mNextLevelButton.x - mResetButton.x), mButtonSize.y);
		this.transform.position = mMainCamera.transform.position;
		mMainCamera.GetComponent<PlayerObjectButtons>().enabled =false;
	}
	
	void OnGUI(){
		
		GUI.Label(mLabelArea, "You Win!");
		
		if(GUI.Button (mResetButton, "Restart Level")){
			Application.LoadLevel(Application.loadedLevel);	
		}
		
		if(GUI.Button (mNextLevelButton, "Next Level")){
			Application.LoadLevel(mNextScene);
		}
	}
			
}
