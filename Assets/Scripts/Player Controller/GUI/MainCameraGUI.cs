﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
	
public class MainCameraGUI : MonoBehaviour {

	public GameObject mBuiltRobotsSprite;
	public GameObject mRobotSpawnLabel;
	public GameObject mGoButton;
	public GameObject mStopButton;
	public int mRobotsNeededToWin;
	public int mMaxRobotsReleased;
	
	public static GameObject mPlayModeCamera;
	public static GameObject mEscapeCamera;
	public static GameObject mEndGameCamera;
	
	private int mCompletedRobots;//Score
	private int mRobotsSpawned;

	private float mStartStopToggleTime;
	private float mStartStopToggleCooldown = 0.5f;

	void Start () {
		mStartStopToggleTime = 0.0f;
		mGoButton.GetComponent<UIButton>().enabled = true;

		mPlayModeCamera = GameObject.FindGameObjectWithTag("PlaymodeCamera").gameObject;
		mEscapeCamera = GameObject.FindGameObjectWithTag("EscapeCamera").gameObject;
		mEndGameCamera = GameObject.FindGameObjectWithTag("EndGameCamera").gameObject;

		mEscapeCamera.SetActive(false);
		mEndGameCamera.SetActive (false);
	}

	//This displays everything that needs to be displayed
	void Update() {

		if(mPlayModeCamera.activeSelf){
			//Assigning all the robot spawning and trcking variables
			mBuiltRobotsSprite.GetComponent<UISprite>().fillAmount = mCompletedRobots/mRobotsNeededToWin;
			mBuiltRobotsSprite.GetComponentInChildren<UILabel>().text = mCompletedRobots + "/" + mRobotsNeededToWin;
			mRobotSpawnLabel.GetComponent<UILabel>().text = (mMaxRobotsReleased - mRobotsSpawned).ToString ();

			mStartStopToggleCooldown += Time.deltaTime;
			CheckForButtonClicks();
			
			if(StartAndReset.mIsGameRunning){
				mStopButton.SetActive(true);
				mGoButton.SetActive(false);
				mStopButton.GetComponent<UIButton>().enabled = true;
			}
			else{
				mStopButton.SetActive(false);
				mGoButton.SetActive(true);
				mGoButton.GetComponent<UIButton>().enabled = true;
			
			}
		}
	}
		
	public bool SpawnRobot(){
		
		if(mRobotsSpawned < mMaxRobotsReleased){
			++mRobotsSpawned;
			return true;
		}
		else{
			return false;	
		}	
	}
	
	public void ScoreRobot(){
		++mCompletedRobots;
		
		if(mCompletedRobots >= mRobotsNeededToWin){
			EndGame();	
		}
	}
	
	public void ResetScore(){
		mCompletedRobots = 0;
		mRobotsSpawned = 0;
	}
	
	void EndGame(){
		mEndGameCamera.SetActive(true);
		gameObject.GetComponent<StartAndReset>().StopGame();
		mPlayModeCamera.SetActive(false);
	}

	void CheckForButtonClicks(){
		
		UIEventListener.Get(mGoButton).onClick += ButtonClicked;
		UIEventListener.Get(mStopButton).onClick += ButtonClicked;
	}
	
	void ButtonClicked(GameObject button){

		if(mStartStopToggleTime > mStartStopToggleCooldown){
			Debug.Log (StartAndReset.mIsGameRunning);
			if(!StartAndReset.mIsGameRunning){
				this.GetComponent<StartAndReset>().StartGame();
			}
			else{
				this.GetComponent<StartAndReset>().StopGame();
			}
			mStartStopToggleTime = 0.0f;
		}
	}

}
