﻿using UnityEngine;
using System.Collections;

public class PlayerObjectButtons : MonoBehaviour {
	
	// Spring then Fan then Trap Conveyor then Conveyor
	public GameObject[] mAvailableObjects;
	public Texture2D[] mObjectTextures;
	public bool[] mEnableEachObject;
	public GameObject[] mButtonLocations;

	private GameObject mCreatedObject;
	private float mPlacingTime;
	private float mPlacingCooldown = 0.5f;
	
	void Start () {
		EnableButtons();
		mPlacingTime = 0.0f;
	}

	void Update() {

		mPlacingTime += Time.deltaTime;
		CheckForButtonClicks();

	}

	void EnableButtons(){

		for(int i = 0; i <mEnableEachObject.Length; i++){
			mButtonLocations[i].SetActive(true);
		}

	}

	void CheckForButtonClicks(){
		
		for(int i = 0; i< mButtonLocations.Length; i++){
			UIEventListener.Get(mButtonLocations[i]).onClick += ButtonClicked;
		}
	}
	
	void ButtonClicked(GameObject button){

		for(int i = 0; i < mAvailableObjects.Length; i++){
			if(mAvailableObjects[i].name + " Button" == button.name){
				PlaceObject(i);
				mPlacingTime = 0.0f;

				break;
			}
		}
	}
	
	public void DeleteObject(string importedName){
		
		int identifier = 5;
		for(int i = 0; i< mAvailableObjects.Length; i++){
			if (importedName == mAvailableObjects[i].name + "(Clone)"){
				identifier = i;	
			}
		}

		this.GetComponent<Budget>().DeleteObject(identifier);

	}
	
	void PlaceObject(int identifier){

		if(mPlacingTime > mPlacingCooldown){
			if(!StartAndReset.mIsGameRunning){
				Vector3 centerOfScreen = gameObject.GetComponent<Camera>().ScreenToWorldPoint(new Vector2(Screen.width/2, Screen.height/2));
				centerOfScreen.z = 0;
					
				if(this.GetComponent<Budget>().AbleToPlace(identifier)){
					return;
				}

				mCreatedObject = Instantiate(mAvailableObjects[identifier], centerOfScreen, Quaternion.identity) as GameObject;
				this.GetComponent<Placer>().AttachPlaceable(mCreatedObject.GetComponent<Placeable>());
				this.GetComponent<Budget>().PlaceObject(identifier);
			}
		}
	}
}
