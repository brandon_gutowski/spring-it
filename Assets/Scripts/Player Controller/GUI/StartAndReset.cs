﻿using UnityEngine;
using System.Collections;

public class StartAndReset : MonoBehaviour {
	
	public GameObject[] mStoredPrefabs;
	
	private GameObject[] mAllObjects;
	private Vector3[] mAllLevelPieces;
	private Quaternion[] mAllLevelPieceRotations;
	private string[] mLevelPieceNames;
	public static bool mIsGameRunning = false;
	
	private GameObject[] mAllMovingParts;
	private string[] mMovingPartNames;


	
	void Start () {


		CreateLevelPieceArray();
		CreateMovingPartsArray();
		
	}

	void CreateLevelPieceArray(){
		mAllObjects = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		mAllLevelPieces = new Vector3[mAllObjects.Length];
		mLevelPieceNames = new string[mAllObjects.Length];
		mAllLevelPieceRotations = new Quaternion[mAllObjects.Length];
		for(int i = 0; i < mAllLevelPieces.Length; i++){
			if(mAllObjects[i].tag == "LevelPiece"){
				mAllLevelPieces[i] = mAllObjects[i].transform.position;
				mAllLevelPieceRotations[i] = mAllObjects[i].transform.rotation;
				mLevelPieceNames[i] = mAllObjects[i].ToString();
			}	
		}
	}
	
	void CreateMovingPartsArray(){
		mAllMovingParts = GameObject.FindGameObjectsWithTag("MovingPart");
		mMovingPartNames = new string[mAllMovingParts.Length];
		for(int i = 0; i < mAllMovingParts.Length; i++){
			mMovingPartNames[i] = mAllMovingParts[i].ToString();
		}
	}
	
	public void StartGame(){
		if(!mIsGameRunning){
			CreateMovingPartsArray();
			CreateLevelPieceArray();
			TogglePlacer(false);
			
			for(int i = 0; i <mAllMovingParts.Length; i++){
				mAllMovingParts[i].GetComponent<MonoBehaviour>().enabled = true;	
			}
			
			for(int i = 0; i<mAllLevelPieces.Length; i++){
				if(mAllObjects[i].GetComponent<MonoBehaviour>() != null){
					mAllObjects[i].GetComponent<MonoBehaviour>().enabled = true;	
				}	
			}
		
			mIsGameRunning = true;
		}
	}
		
	public void StopGame(){
		if(mIsGameRunning){
			DeleteNonPlaceables();
			DeleteRobots();
			CreateObjects();
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MainCameraGUI>().ResetScore();
			mIsGameRunning = false;
			TogglePlacer(true);
		}
	}
	
	void DeleteNonPlaceables(){
		GameObject[] allObjects = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		for(int i = 0; i <allObjects.Length; i++){
			if(allObjects[i].tag == "LevelPiece"){
				Destroy(allObjects[i]);
			}
		}	
	}
	
	void DeleteRobots(){
		GameObject[] allRobots = GameObject.FindGameObjectsWithTag("Robot");

		for(int i = 0; i < allRobots.Length; i++){
			Destroy(allRobots[i]);
		}	
	}
	
	void CreateObjects(){
		for(int i = 0; i < mAllLevelPieces.Length; i++){
			for(int j = 0; j < mStoredPrefabs.Length;j++){
				if(mLevelPieceNames[i] == mStoredPrefabs[j].ToString()){
					GameObject tempObject = Instantiate(mStoredPrefabs[j], mAllLevelPieces[i], mAllLevelPieceRotations[i]) as GameObject;
					tempObject.name = mStoredPrefabs[j].name;
				}
			}
		}
			
	}
	
	void TogglePlacer(bool state){
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Placer>().enabled = state;
		
	}

	public bool IsGameRunning(){
		return mIsGameRunning;
	}
}
