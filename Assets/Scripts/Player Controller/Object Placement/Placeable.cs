﻿using UnityEngine;
using System.Collections;

public class Placeable : MonoBehaviour 
{
	public float m_RotationDegrees;
	
	private Vector2 m_SnapSize;
			
	private bool m_IsAttachedToMouse = false;
	private Camera m_Camera;
	private Transform m_CameraTransform;
	private Transform m_Tranform;
	private float mTimeSinceCreation = 0.0f;
	
	void Start () 
	{
		m_Camera = Camera.main;
		m_CameraTransform = m_Camera.transform;
		m_Tranform = transform;		
	}
	
	void Update () 
	{	
		mTimeSinceCreation += Time.deltaTime;
		
		if(m_IsAttachedToMouse) 
		{	
			Vector3 mousePosition = Input.mousePosition;
			mousePosition.z = m_Tranform.position.z - m_CameraTransform.position.z;
			mousePosition = m_Camera.ScreenToWorldPoint(mousePosition);

			if(!m_Camera.GetComponent<Placer>().IsObjectRotating())
			{
				m_Tranform.position = this.CalculateNewPosition(mousePosition);
			}		
		}
	}
	
	public void AttachToMouse(Vector2 snapSize) 
	{
		m_SnapSize = snapSize;
		m_IsAttachedToMouse = true;
	}
	
	public void Place(Vector3 mouseLocation)
	{		
		m_IsAttachedToMouse = false;
	}
		
	private Vector3 CalculateNewPosition(Vector2 mousePosition)
	{
		if(this.m_SnapSize.x == 0 || this.m_SnapSize.y == 0)
		{
			return new Vector3(mousePosition.x, mousePosition.y, m_Tranform.position.z);
		}

		Vector2 remainder = new Vector2(mousePosition.x % m_SnapSize.x, mousePosition.y % m_SnapSize.y);
		Vector2 offset = new Vector2(0, 0);
		
		if(remainder.x >= m_SnapSize.x / 2)
		{
			offset.x = m_SnapSize.x;
		}
		
		if(remainder.y >= m_SnapSize.y / 2)
		{
			offset.y = m_SnapSize.y;
		}
		
		Vector3 newPosition = m_Tranform.position;
		newPosition.x = mousePosition.x - remainder.x + offset.x;
		newPosition.y = mousePosition.y - remainder.y + offset.y;
		
		return newPosition;
	}
	
	public void RotateRight(bool turnRight)
	{
		if(m_RotationDegrees == 180){		
			transform.Rotate (0, m_RotationDegrees, 0);

		}
		else{
			if(turnRight)
			{
				transform.Rotate(0,0, -m_RotationDegrees); 
			}
			else
			{
				transform.Rotate(0,0, m_RotationDegrees);
			}
		}
		
	}
	
	public float TimeSinceCreation(){
		return mTimeSinceCreation;	
	}
}
