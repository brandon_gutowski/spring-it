﻿using UnityEngine;
using System.Collections;

public class Placer : MonoBehaviour 
{
	public Vector2 m_SnapSize;
	public float m_MouseHoldLimit = 0.20f;
	public Texture2D m_RotateCCWImage;
	public Texture2D m_RotateCWImage;
	public float m_ButtonDistanceFromObject;
	public float m_DistanceToCancelRotation;
	
	
	private float m_MouseTimer;
	private Placeable m_CurrentObject;
	private Camera m_Camera;
	
	private bool ObjectAttached { get { return m_CurrentObject != null; } }
	
	private bool m_IsRotating = false;
	private Rect m_ClockwiseRect;
	private Rect m_CounterClockwiseRect;
	
	void Start () 
	{
		m_Camera = Camera.main;	
	}
	
	void Update () 
	{	
		if(Input.GetMouseButtonDown(0)) 
		{
			
			Ray screenRay = m_Camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(screenRay, out hit))
			{
				if(hit.collider.tag != "MovingPart")
				{
					Transform curTransform = hit.transform;
					do
					{
						AttachPlaceable(curTransform.GetComponent<Placeable>());				
						curTransform = curTransform.parent;
					}
					while(curTransform != null && !ObjectAttached);
				}

			}
		}
		
		if(ObjectAttached)
		{
			if(!m_IsRotating)
			{
				m_MouseTimer += Time.deltaTime;
			}
			else
			{
				m_MouseTimer = 0.0f;
			}
		}

		CheckForMouseRelease();
		
		CheckToDeleteSelectedPlaceable();
		
	}
	
	public void CheckToDeleteSelectedPlaceable()
	{
		
		if(Input.GetMouseButtonDown(1) )
		{
			if(ObjectAttached)
			{
				this.GetComponent<PlayerObjectButtons>().DeleteObject(m_CurrentObject.name);
				Destroy(m_CurrentObject.gameObject);
				m_CurrentObject = null;
				m_IsRotating = false;
			}
		}
	}
	
	public void CheckForMouseRelease()
	{
		if(Input.GetMouseButtonUp(0))
		{
			if(ObjectAttached){
				if(((m_MouseTimer - m_MouseHoldLimit) < 0) && (m_CurrentObject.TimeSinceCreation() > 1.0f))
				{
					m_IsRotating = true;
					m_MouseTimer = 0.0f;
				}
				else
				{
					if(!m_IsRotating)
					{
						DetachCurrentObject();
						m_MouseTimer = 0.0f;
					}
				}
			}
		}	
	}	
	
	public void AttachPlaceable(Placeable newPlaceable) 
	{
		if(newPlaceable == null){
			return;
		}
		
		m_CurrentObject = newPlaceable;
		m_CurrentObject.AttachToMouse(m_SnapSize);
	}
	
	public void DetachCurrentObject() 
	{
		if(!ObjectAttached)
			return;
		
		Vector3 mouseLocation = m_Camera.ScreenToWorldPoint(Input.mousePosition);
		m_CurrentObject.Place(mouseLocation);
		m_CurrentObject = null;
	}
	
	public void OnGUI()
	{
		if(m_IsRotating)
		{
			Vector2 currentObjectScreenLocation = CalculateButtonLocations();


			if(GUI.Button(m_CounterClockwiseRect, m_RotateCCWImage))
			{
				m_CurrentObject.RotateRight(false);
				
			}
			
			if(GUI.Button(m_ClockwiseRect, m_RotateCWImage))
			{
				m_CurrentObject.RotateRight(true);
			}
			
			if(Input.GetMouseButtonDown(0) && (Mathf.Abs(Input.mousePosition.magnitude - currentObjectScreenLocation.magnitude) >= m_DistanceToCancelRotation))
			{
				m_CurrentObject.Place (this.transform.position);
				m_CurrentObject = null;
				m_IsRotating = false;
			}
		}
	}
	
	Vector2 CalculateButtonLocations()
	{
		//Vector3 topOfBox = m_CurrentObject.transform.position;
		//topOfBox.y += m_CurrentObject.GetComponent<MeshRenderer>().bounds.extents.y;
		
		Vector2 currentObjectScreenLocation = m_Camera.WorldToScreenPoint(m_CurrentObject.transform.position);
		//Vector2 currentObjectTopLocation = m_Camera.WorldToScreenPoint(topOfBox);
		//float heightOfObjectOnScreen = currentObjectTopLocation.y + currentObjectScreenLocation.y;
		
		m_ClockwiseRect = new Rect(currentObjectScreenLocation.x - 2.5f * m_ButtonDistanceFromObject, Screen.height - currentObjectScreenLocation.y + m_ButtonDistanceFromObject, 50, 50);
		m_CounterClockwiseRect = new Rect(currentObjectScreenLocation.x + m_ButtonDistanceFromObject, Screen.height - currentObjectScreenLocation.y - 2f * m_ButtonDistanceFromObject, 50, 50);

		return currentObjectScreenLocation;
	}
	
	public bool IsObjectRotating()
	{
		return m_IsRotating;	
	}

}
